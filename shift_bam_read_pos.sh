#!/bin/bash
#
# Shell wrapper ; expects file.bam fwd_shift rev_shift out.bam
BAM_FILE="in.bam"
FWD_SHIFT=$2
REV_SHIFT=$3
OUT_BAM=$4

samtools --version

ln -s $1 $BAM_FILE && cat \
<(samtools view -H $BAM_FILE) \
<(samtools view -F 16 $BAM_FILE | awk -v OFS="\t" -v FoS=$FWD_SHIFT -v ReV=$REV_SHIFT '{$4=$4+FoS ; if($8>0 && and($2,2)){ if(and($2,32)){$8=$8+ReV}else{$8=$8+FoS} }; print $0}') \
<(samtools view -f 16 $BAM_FILE | awk -v OFS="\t" -v FoS=$FWD_SHIFT -v ReV=$REV_SHIFT '{$4=$4+ReV ; if($8>0 && and($2,2)){ if(and($2,32)){$8=$8+ReV}else{$8=$8+FoS} }; print $0}') | \
samtools view -bS - | samtools sort -@ \${GALAXY_SLOTS:-2} -o $OUT_BAM
