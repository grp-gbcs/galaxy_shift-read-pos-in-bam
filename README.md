The tool offsets read position by given value (can be negative) and creates a 
sorted BAM output. 

Important notes for installion :
* The single line linux will anyway run 3 sub-processes in paralell so you better 
make sure the tool requires 3+1 (for samtools sort) cpus on the same machine
